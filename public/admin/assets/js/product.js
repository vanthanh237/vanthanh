$(document).ready(function (e) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function getSuccessAlert(data) {
        alert(data);
        location.reload();
    }

    function getErrorsForCreate(errors) {
        if ($.isEmptyObject(errors) == false) {
            $.each(errors.errors, function (key, value) {
                let errorID = "#" + key + '-' + 'error';
                $(errorID).text(value);
            })
        }
    }

    function getErrorsForUpdate(errors) {
        if ($.isEmptyObject(errors) == false) {
            $.each(errors.errors, function (key, value) {
                let errorID = "#" + key + '-' + 'update' + '-' + 'error';
                $(errorID).text(value);
            })
        }
    }

    $('#form-create-product').submit(function (e) {
        e.preventDefault();
        let formData = new FormData(this);//
        let url = $(this).attr('data-url');
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status_code === 200) {
                    $('#create-product').modal('hide');
                    getSuccessAlert(data.message);
                }
            },
            error: function (data) {
                let errors = data.responseJSON;
                getErrorsForCreate(errors);
            }
        })
    })

    $('.btn-edit').click(function (e) {

        let url = $(this).attr('data-url');

        $('#update-product').modal('show');
        e.preventDefault();

        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                $('#id-product-edit').val(response.product.id);
                $('#name-edit').val(response.product.name);
                $('#price-edit').val(response.product.price);
                $('#description-edit').val(response.product.description);
                $('#category-id-edit').val(response.product.category_id);
            },
        })
    })

    $('#form-update-product').submit(function (e) {
        e.preventDefault();
        let formData = new FormData(this);
        let url = $(this).attr('data-url');
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status_code === 200) {
                    $('#update-product').modal('hide');
                    getSuccessAlert(data.message);
                }
            },
            error: function (data) {
                let errors = data.responseJSON;
                getErrorsForUpdate(errors);
            }
        })
    })

    $('.btn-delete').click(function () {
        let url = $(this).attr('data-url');
        if (confirm("Are you sure you want to delete?")) {
            $.ajax({
                type: 'DELETE',
                url: url,
                success: function () {
                    location.reload();
                },
                error: (e) => {
                    console.log(e);
                    $('body').html(e.responseText);
                }
            })
        }
    })
})




