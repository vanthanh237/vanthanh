<?php

namespace App\Services;

use App\Repositories\ProductRepository;

class ProductService
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAll()
    {
        return $this->productRepository->getAll();
    }

    public function search()
    {
        return $this->productRepository->searchProducts();
    }

    public function getItemById($id)
    {
        return $this->productRepository->getItemById($id);
    }

    public function store($request)
    {
        $this->upLoadImage($request);
        return $this->productRepository->store($request->all());
    }

    public function update($request)
    {
        $this->upLoadImage($request);
        return $this->productRepository->updateProduct($request->all());
    }

    public function delete($id)
    {
        return $this->productRepository->delete($id);
    }

    public function upLoadImage($request)
    {
        if ($request->hasFile('fileThumbnail')) {
            $file = $request->file('fileThumbnail');
            $extention = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extention;
            $file->move('uploads/products/', $fileName);
            $request['thumbnail'] = $fileName;
        }
        return $request;
    }
}
