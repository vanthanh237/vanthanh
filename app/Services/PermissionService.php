<?php

namespace App\Services;

use App\Repositories\PermissionRepository;

class PermissionService
{
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function getAll()
    {
        return $this->permissionRepository->getAll();
    }

    public function getItemById($id)
    {
        return $this->permissionRepository->getItemById($id);
    }

    public function store($request)
    {
        return $this->permissionRepository->store($request->all());
    }

    public function update($request, $id)
    {
        return $this->permissionRepository->update($request->all(), $id);
    }

    public function delete($id)
    {
        return $this->permissionRepository->delete($id);
    }
}
