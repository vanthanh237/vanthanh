<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getAll()
    {
        return $this->roleRepository->getAll();
    }

    public function getItemById($id)
    {
        return $this->roleRepository->getItemById($id);
    }

    public function store($request)
    {
        return $this->roleRepository->store($request);
    }

    public function update($request, $id)
    {
        return $this->roleRepository->update($request, $id);
    }

    public function delete($id)
    {
        return $this->roleRepository->delete($id);
    }

}
