<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getAll()
    {
        return $this->categoryRepository->getAll();
    }

    public function getItemById($id)
    {
        return $this->categoryRepository->getItemById($id);
    }

    public function store($request)
    {
        return $this->categoryRepository->store($request->all());
    }

    public function update($request, $id)
    {
        return $this->categoryRepository->update($request->all(), $id);
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);

    }
}
