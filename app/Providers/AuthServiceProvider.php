<?php

namespace App\Providers;

use App\Policies\CategoryPolicy;
use App\Policies\ProductPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;
use App\Policies\PermissionPolicy;
use App\Policies\RolePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->userGate();
        $this->permissionPolicies();
        $this->rolePolicies();
        $this->categoryPolicies();
        $this->productPolicies();
    }

    public function userGate()
    {
        Gate::define('manage-users',function ($user){
            return $user->hasAnyRoles(['admin','visitor']);
        });

        Gate::define('actions-user',function ($user){
            return $user->hasAnyRoles(['admin']);
        });
    }

    public function permissionPolicies()
    {
        Gate::define("permission.view", [PermissionPolicy::class, "view"]);
        Gate::define("permission.create", [PermissionPolicy::class, "create"]);
        Gate::define("permission.update", [PermissionPolicy::class, "update"]);
        Gate::define("permission.delete", [PermissionPolicy::class, "delete"]);
    }

    public function rolePolicies()
    {
        Gate::define("role.view", [RolePolicy::class, "view"]);
        Gate::define("role.create", [RolePolicy::class, "create"]);
        Gate::define("role.update", [RolePolicy::class, "update"]);
        Gate::define("role.delete", [RolePolicy::class, "delete"]);
    }

    public function categoryPolicies()
    {
        Gate::define("category.view", [CategoryPolicy::class, "view"]);
        Gate::define("category.create", [CategoryPolicy::class, "create"]);
        Gate::define("category.update", [CategoryPolicy::class, "update"]);
        Gate::define("category.delete", [CategoryPolicy::class, "delete"]);
    }

    public function productPolicies()
    {
        Gate::define("product.view", [ProductPolicy::class, "view"]);
        Gate::define("product.create", [ProductPolicy::class, "create"]);
        Gate::define("product.update", [ProductPolicy::class, "update"]);
        Gate::define("product.delete", [ProductPolicy::class, "delete"]);
    }
}
