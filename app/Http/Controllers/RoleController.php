<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Services\PermissionService;
use App\Services\RoleService;

class RoleController extends Controller
{
    protected $roleService;
    protected $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function index()
    {
        $roles = $this->roleService->getAll();
        return view('roles.index', compact('roles'));
    }

    public function create()
    {
        $permissions = $this->permissionService->getAll();
        return view('roles.create', compact('permissions'));
    }

    public function store(RoleRequest $request)
    {
        $this->roleService->store($request);
        return redirect()->route('roles.index');
    }

    public function edit($id)
    {
        $role = $this->roleService->getItemById($id);
        $permissions = $this->permissionService->getAll();
        return view('roles.update', compact('role', 'permissions'));
    }

    public function update(RoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect()->route('roles.index');

    }

    public function destroy($id)
    {
        $this->roleService->delete($id);
        return redirect()->route('roles.index');
    }
}
