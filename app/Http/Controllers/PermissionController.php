<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermissionRequest;
use App\Services\PermissionService;

class PermissionController extends Controller
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function index()
    {
        $permissions = $this->permissionService->getAll();
        return view('permissions.index', compact('permissions'));
    }

    public function create()
    {
        return view('permissions.create');
    }

    public function store(PermissionRequest $request)
    {
        $this->permissionService->store($request);
        return redirect()->route('permissions.index');
    }

    public function edit($id)
    {
        $permission = $this->permissionService->getItemById($id);
        return view('permissions.update', compact('permission'));
    }

    public function update(PermissionRequest $request, $id)
    {
        $this->permissionService->update($request, $id);
        return redirect()->route('permissions.index');

    }

    public function destroy($id)
    {
        $this->permissionService->delete($id);
        return redirect()->route('permissions.index');
    }
}
