<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Services\CategoryService;
use App\Services\ProductService;

class ProductController extends Controller
{
    protected $productService;
    protected $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $products = $this->productService->search();
        $categories = $this->categoryService->getAll();
        return view('products.index', compact('products', 'categories'));
    }

    public function store(ProductRequest $request)
    {
        $product = $this->productService->store($request);
        return response()->json([
            'status_code' => 200,
            'product' => $product,
            'message' => "Create new product success!"
        ]);
    }

    public function edit($id)
    {
        $product = $this->productService->getItemById($id);
        $categories = $this->categoryService->getAll();
        return response()->json([
            'status_code' => 200,
            'product' => $product,
            'categories' => $categories
        ]);
    }

    public function update(ProductRequest $request)
    {
        $product = $this->productService->update($request);
        return response()->json([
            'status_code' => 200,
            'product' => $product,
            'message' => "Update product success!"
        ]);
    }

    public function destroy($id)
    {
        $this->productService->delete($id);
        return response()->json([
            'status_code' => 200,
        ]);
    }
}
