<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->hasPermission(['category.view']);
    }

    public function create(User $user)
    {
        return $user->hasPermission(['category.create']);
    }

    public function update(User $user)
    {
        return $user->hasPermission(['category.update']);
    }

    public function delete(User $user)
    {
        return $user->hasPermission(['category.delete']);
    }
}
