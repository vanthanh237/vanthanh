<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }

    public function store($request)
    {
        $role = $this->model->create($request->all());
        $role->permissions()->attach($request->permissions);
        return $role;
    }

    public function update($request, $id)
    {
        $role = $this->getItemById($id);
        $role->permissions()->sync($request->permissions);
        return $role->update($request->all());
    }

    public function delete($id)
    {
        $role = $this->getItemById($id);
        $role->permissions()->detach($role);
        $role->delete();
        return $role;
    }

}
