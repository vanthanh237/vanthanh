<?php

namespace App\Repositories;

abstract class BaseRepository
{
    protected $model;

    abstract public function model();

    public function makeModel()
    {
        $this->model = app($this->model());
    }

    public function __construct()
    {
        $this->makeModel();
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getItemById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function paginate()
    {
        return $this->model->paginate(10);
    }

    public function store($data)
    {
        return $this->model->create($data);
    }

    public function update($request, $id)
    {
        $data = $this->getItemById($id);
        return $data->update($request);
    }

    public function delete($id)
    {
        $data = $this->getItemById($id);
        $data->delete();
        return $data;
    }

}
