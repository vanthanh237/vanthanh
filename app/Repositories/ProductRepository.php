<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function searchProducts()
    {
        return $this->model->whereHas('categories', function ($query) {
            $query->SearchCategory();})
            ->searchProductName()
            ->latest('id')
            ->paginate(5)
            ->appends(request()->query());
    }

    public function updateProduct($request)
    {
        $data = $this->getItemById($request['id']);
        return $data->update($request);
    }
}
