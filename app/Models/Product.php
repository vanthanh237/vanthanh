<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'price',
        'description',
        'thumbnail',
        'category_id'
    ];

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function scopeSearchProductName($query)
    {
        if(!empty(request()->key)){
          $query->where('name', 'like', '%' . request()->key . '%');
        }
        return $query;
    }
}
