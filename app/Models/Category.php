<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    public $timestamps = false;
    protected $fillable = [
        'name'
    ];

    public function hasManyProduct(){
        return $this->hasMany(Product::class, 'category_id', 'id');
    }

    public function scopeSearchCategory($query)
    {
        if(!empty(request()->category_id)){
            $query->where('id', request()->category_id);
        }
        return $query;
    }
}
