<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';
    public $timestamps = false;
    protected $fillable = [
        'name'
    ];

    public function users(){
        return $this->belongsToMany(User::class,'user_role','role_id','user_id');
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class,'role_permission','role_id','permission_id');
    }

    public function hasPermission($permissions)
    {
        foreach($permissions as $permission)
        {
            if($this->permissions->contains('name', $permission)){
                return true;
            }
        }
        return false;
    }

}
