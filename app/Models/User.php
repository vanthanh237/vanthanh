<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    public $timestamps = false;
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role', 'user_id', 'role_id');
    }

    public function hasAnyRoles($roles)
    {
        if ($this->roles()->whereIn('name', $roles)->first()) {
            return true;
        }
        return false;
    }

    public function hasAnyPermissions($perrmissions)
    {
        foreach ($this->roles as $role) {
            if ($role->permissions()->whereIn('name', $perrmissions)->first()) {
                return true;
            }
        }
        return false;
    }

    public function hasRoles($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->roles->contains('name', $role)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function hasPermission($perrmissions)
    {
        if (is_array($perrmissions)) {
            foreach ($this->roles as $role) {
                if ($role->hasPermission($perrmissions)) {
                    return true;
                }
            }
        }
        return false;
    }

}
