@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">All Categories</div>
                    <div class="card-body mb-3">
                        @hasPermission(['category.create'])
                            <div class="mb-3">
                                <a href="{{route('categories.create')}}" class="btn btn-primary">Create New Category</a>
                            </div>
                        @endhasPermission
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                @hasPermission(['category.update','category.delete'])
                                <th scope="col">Actions</th>
                                @endhasPermission
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $key => $category)
                                <tr>
                                    <th scope="row">{{++$key}}</th>
                                    <td>{{$category->name}}</td>
                                    <td>
                                        @hasPermission(['category.update'])
                                            <a href="{{route('categories.edit', $category->id)}}">
                                                <button type="button" class="btn btn-warning float-left">Edit</button>
                                            </a>
                                        @endhasPermission
                                        @hasPermission(['category.delete'])
                                            <form action="{{route('categories.destroy', $category->id)}}" method="POST"
                                                  class="float-left">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        @endhasPermission
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
