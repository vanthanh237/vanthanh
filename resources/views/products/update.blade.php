<!-- Modal -->
<div class="modal fade" id="update-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-update-product" data-url="{{ route('products.update') }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" id="id-product-edit">
                <div class="card-body">
                    <div class="mb-3">
                        <input type="text" id="name-edit" class="form-control" name="name"
                               placeholder="Name..."/>
                        <p class="text-danger" id="name-update-error"></p>
                    </div>
                    <div class="mb-3">
                        <input type="number" id="price-edit" class="form-control" name="price"
                               placeholder="Price..."/>
                        <p class="text-danger" id="price-update-error"></p>
                    </div>
                    <div class="mb-3">
                        <textarea id="description-edit" class="form-control" rows="3" name="description"
                                  placeholder="Descriptions...">
                        </textarea>
                        <p class="text-danger" id="description-update-error"></p>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Category</label>
                        <select id="category-id-edit" class="form-select form-control" name="category_id">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Image product</label>
                        <input id="file-thumbnail-edit" class="form-control" type="file" name="fileThumbnail"/>
                        <p class="text-danger" id="fileThumbnail-update-error"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

