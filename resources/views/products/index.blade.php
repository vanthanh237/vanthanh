@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">All Products</div>
                    <div class="card-body">
                        @hasPermission(['product.create'])
                        <div class="mb-3">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#create-product">
                                Create New Product
                            </button>
                        </div>
                        @endhasPermission
                        <div class="mb-3">
                            <form action="{{ route('products.index') }}" method="GET" class="d-flex">
                                <input type="text" class="form-control" name="key" value="{{request('key')}}">
                                <select name="category_id" class="col-md-3 form-control">
                                    <option value="0">Select category</option>
                                    @foreach ($categories as $category)
                                        <option
                                            value="{{ $category->id }}" {{ request('category_id') == $category->id ? "selected":"" }}>{{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <button class="btn btn-primary" type="submit">Search</button>
                            </form>
                        </div>
                        <div id="table-list">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Category</th>
                                    @hasPermission(['product.update','product.delete'])
                                    <th scope="col">Actions</th>
                                    @endhasPermission
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $key => $product)
                                    <tr>
                                        <th scope="row">{{++$key}}</th>
                                        <td>{{$product->name}}</td>
                                        <td>{{number_format($product->price)}} VND</td>
                                        <td>{{$product->description}}</td>
                                        <td>
                                            <img class="img-responsive img-rounded"
                                                 style="max-height: 100px; max-width: 100px;"
                                                 src="{{url('uploads/products/'.$product->thumbnail)}}"
                                                 alt="{{$product->name}}">
                                        </td>
                                        <td>{{$product->categories->name}}</td>
                                        @hasPermission(['product.update','product.delete'])
                                        <td>
                                            @hasPermission(['product.update'])
                                            <button data-url="{{route('products.edit', $product->id)}}" type="button"
                                                    class="btn btn-warning btn-edit">
                                                Edit
                                            </button>
                                            @endhasPermission
                                            @hasPermission(['product.delete'])
                                            <button data-url="{{route('products.destroy', $product->id)}}" type="button"
                                                    class="btn btn-danger btn-delete">
                                                Delete
                                            </button>
                                            @endhasPermission
                                        </td>
                                        @endhasPermission
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$products->links()}}
                    </div>
                </div>
                {{-- Modal for create product --}}
                @include('products.create')
                {{-- Modal for update product --}}
                @include('products.update')
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script defer src="{{ asset('admin/assets/js/product.js') }}" type="text/javascript"></script>
@endsection
