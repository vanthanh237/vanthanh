<!-- Modal -->
<div class="modal fade" id="create-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-create-product" data-url="{{ route('products.store') }}" method="POST" role="form"
                  enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="mb-3">
                        <input type="text" id="name" class="form-control" name="name" placeholder="Name..."/>
                        <p class="text-danger" id="name-error"></p>
                    </div>
                    <div class="mb-3">
                        <input type="number" id="price" step="any" class="form-control" name="price"
                               placeholder="Price..."/>
                        <p class="text-danger" id="price-error"></p>
                    </div>
                    <div class="mb-3">
                        <textarea class="form-control" id="description" rows="3" name="description"
                                  placeholder="Descriptions...">
                        </textarea>
                        <p class="text-danger" id="description-error"></p>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Category</label>
                        <select class="form-select form-control" id="category-id" name="category_id">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Image product</label>
                        <input class="form-control" id="file-thumbnail" type="file" name="fileThumbnail"/>
                        <p class="text-danger" id="fileThumbnail-error"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
