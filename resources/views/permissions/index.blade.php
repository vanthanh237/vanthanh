@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">All Permissions</div>
                    <div class="card-body mb-3">
                        @hasPermission(['permission.create'])
                        <div class="mb-3">
                            <a href="{{route('permissions.create')}}" class="btn btn-primary">Create New
                                Permission</a>
                        </div>
                        @endhasPermission
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                @hasRole(['admin'])
                                <th scope="col">Actions</th>
                                @endhasRole
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($permissions as $key => $permission)
                                <tr>
                                    <th scope="row">{{++$key}}</th>
                                    <td>{{$permission->name}}</td>
                                    @hasRole(['admin'])
                                    <td>
                                        <a href="{{route('permissions.edit', $permission->id)}}">
                                            <button type="button" class="btn btn-warning float-left">Edit</button>
                                        </a>
                                        <form action="{{route('permissions.destroy', $permission->id)}}"
                                              method="POST"
                                              class="float-left">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                    @endhasRole
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
