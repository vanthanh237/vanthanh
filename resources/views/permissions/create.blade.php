@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create Permissions</div>

                    <div class="card-body">
                        <form action="{{route('permissions.store')}}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control" name="name" placeholder="Name...">
                                    @error('name')
                                    <span id="name-error" class="text-danger" style="display: block">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                                <input type="submit" class="btn btn-success" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
