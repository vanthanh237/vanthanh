@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create Roles</div>
                    <div class="card-body">
                        <form action="{{route('roles.store')}}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <input type="text" class="form-control" name="name" placeholder="Name...">
                                @error('name')
                                <span id="name-error" class="text-danger" style="display: block">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            @foreach($permissions as $permission)
                                <div class="form-check">
                                    <input type="checkbox" name="permissions[]"
                                           value="{{$permission->id}}">
                                    <label for="">{{$permission->name}}</label>
                                </div>
                            @endforeach
                            <input type="submit" class="btn btn-success" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
