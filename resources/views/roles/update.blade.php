@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Role</div>

                    <div class="card-body">
                        <form action="{{route('roles.update', $role->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <input type="text" class="form-control" name="name" placeholder="Name..."
                                       value="{{$role->name}}">
                                @error('name')
                                <span id="name-error" class="text-danger" style="display: block">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            @foreach($permissions as $permission)
                                <div class="form-check">
                                    <input type="checkbox" name="permissions[]" value="{{$permission->id}}"
                                           @if ($role->permissions->pluck('id')->contains($permission->id)) checked @endif>
                                    <label for="">{{$permission->name}}</label>
                                </div>
                            @endforeach
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
