<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    public function getCreateCategoryRoute()
    {
        return route('categories.store');
    }

    public function getCreatecategoryViewRoute()
    {
        return route('categories.create');
    }

    public function getIndexRoute()
    {
        return route('categories.index');
    }

    /** @test */
    public function user_isnt_admin_cant_see_create_product_categories_button()
    {
        $this->loginAsVisitorRole();

        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Create New Category');
    }

    /** @test */
    public function user_is_admin_can_see_create_product_categories_button()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertSeeText('Create New Category');
    }

    /** @test */
    public function admin_is_admin_can_view_create_category_form()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getCreatecategoryViewRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.create');
    }

    /** @test */
    public function user_is_admin_can_not_create_category_if_name_feild_is_null()
    {
        $this->loginAsAdminRole();
        $category = Category::factory()->make(['name' => null]);
        $response = $this->post($this->getCreateCategoryRoute(), $category->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
}
