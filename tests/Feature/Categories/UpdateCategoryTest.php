<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('categories.index');
    }

    public function getViewUpdateRoute($id)
    {
        return route('categories.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('categories.update', $id);
    }

    /** @test */
    public function user_isnt_admin_cant_see_edit_button_category()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertDontSeeText('Edit');
    }

    /** @test */
    public function user_is_admin_can_see_edit_button_category()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText('Edit');
    }

    /** @test */
    public function user_is_admin_can_update_the_category()
    {
        $this->loginAsAdminRole();
        $category = Category::factory()->create();
        $category->name = "Updated Title Category";
        $this->put($this->getUpdateRoute($category->id), $category->toArray());
        $this->assertDatabaseHas('categories', ['id' => $category->id, 'name' => 'Updated Title Category']);
    }

    /** @test */
    public function user_is_admin_can_view_update_category_form()
    {
        $this->loginAsAdminRole();
        $category = Category::factory()->create();
        $response = $this->get($this->getViewUpdateRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.update');
    }

    /** @test */
    public function user_isnt_admin_can_not_see_update_category_form_view()
    {
        $this->loginAsVisitorRole();
        $category = Category::factory()->create();
        $response = $this->get($this->getViewUpdateRoute($category->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
