<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('categories.destroy', $id);
    }

    public function getIndexRoute()
    {
        return route('categories.index');
    }

    /** @test */
    public function user_isnt_admin_cant_see_delete_button_category()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertDontSeeText('Delete');
    }

    /** @test */
    public function user_is_admin_can_see_delete_button_category()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText('Delete');
    }

    /** @test */
    public function admin_can_delete_a_category()
    {
        $this->loginAsAdminRole();
        $category = Category::factory()->create();
        $this->delete($this->getDeleteRoute($category->id));
        $this->assertDatabaseMissing('categories', $category->toArray());
    }

    /** @test */
    public function is_not_admin_cant_delete_a_category()
    {
        $this->loginAsVisitorRole();
        $category = Category::factory()->create();
        $response = $this->delete($this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('categories', $category->toArray());
    }
}
