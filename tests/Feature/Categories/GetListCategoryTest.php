<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    public function getListCategoryRoute()
    {
        return route('categories.index');
    }

    /** @test */
    public function user_can_get_all_category()
    {
        $this->loginAsVisitorRole();
        $category = Category::factory()->create();
        $response = $this->get($this->getListCategoryRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
        $response->assertSee($category->name);
    }
}
