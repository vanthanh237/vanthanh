<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('products.index');
    }

    public function getViewUpdateRoute($id)
    {
        return route('products.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('products.update', $id);
    }

    /** @test */
    public function user_isnt_admin_cant_see_edit_button_product()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertDontSeeText('Edit');
    }

    /** @test */
    public function user_is_admin_can_see_edit_button_product()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText('Edit');
    }

    /** @test */
    public function user_isnt_admin_can_not_see_update_product_form_view()
    {
        $this->loginAsVisitorRole();
        $product = Product::factory()->create();
        $response = $this->get($this->getViewUpdateRoute($product->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
