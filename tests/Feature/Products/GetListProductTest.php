<?php

namespace Tests\Feature\Products;

use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('products.index');
    }

    /** @test */
    public function any_user_can_see_product_list()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
    }
}
