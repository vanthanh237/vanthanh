<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('products.destroy', $id);
    }

    public function getIndexRoute()
    {
        return route('products.index');
    }

    /** @test */
    public function admin_can_delete_a_product()
    {
        $this->loginAsAdminRole();
        $product = Product::factory()->create();
        $this->delete($this->getDeleteRoute($product->id));
        $this->assertDatabaseMissing('products', $product->toArray());
    }

    /** @test */
    public function is_not_admin_cant_delete_a_product()
    {
        $this->loginAsVisitorRole();
        $product = Product::factory()->create();
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertStatus(403);
        $this->assertDatabaseHas('products', $product->toArray());
    }

    /** @test */
    public function user_isnt_admin_cant_see_delete_button_product()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertDontSeeText('Delete');
    }

    /** @test */
    public function user_is_admin_can_see_delete_button_product()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText('Delete');
    }
}
