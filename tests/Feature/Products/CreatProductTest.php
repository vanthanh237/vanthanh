<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreatProductTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('products.index');
    }

    public function getCreateProductRoute()
    {
        return route('products.store');
    }

    public function getCreateProductViewRoute()
    {
        return route('products.create');
    }

    /** @test */
    public function user_isnt_admin_cant_see_create_products_button()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Create New Product');
    }

    /** @test */
    public function user_is_admin_can_see_create_products_button()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertSeeText('Create New Product');
    }

    /** @test */
    public function user_is_admin_can_not_create_product_if_name_feild_is_null()
    {
        $this->loginAsAdminRole();
        $product = Product::factory()->make(['name' => null]);
        $response = $this->post($this->getCreateProductRoute(), $product->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
}
