<?php

namespace Tests\Feature\Permissions;

use App\Models\Permission;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreatePermissionTest extends TestCase
{
    public function getCreatePermissionRoute()
    {
        return route('permissions.store');
    }

    public function getCreatePermissionViewRoute()
    {
        return route('permissions.create');
    }

    public function getIndexRoute()
    {
        return route('permissions.index');
    }

    /** @test */
    public function user_isnt_admin_cant_see_create_permission_button()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Create New Permission');
    }

    /** @test */
    public function user_is_admin_can_new_permission()
    {
        $this->loginAsAdminRole();
        $permission = Permission::factory()->make()->toArray();
        $response = $this->post($this->getCreatePermissionRoute(), $permission);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('permissions', $permission);
        $response->assertRedirect($this->getIndexRoute());
    }

    /** @test */
    public function admin_is_admin_can_view_create_permission_form()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getCreatePermissionViewRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('permissions.create');
    }

    /** @test */
    public function user_is_admin_can_not_create_permission_if_name_feild_is_null()
    {
        $this->loginAsAdminRole();
        $permission = Permission::factory()->make(['name' => null]);
        $response = $this->post($this->getCreatePermissionRoute(), $permission->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
}
