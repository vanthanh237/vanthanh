<?php

namespace Tests\Feature\Permissions;

use App\Models\Permission;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeletePermissionTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('permissions.destroy', $id);
    }

    public function getIndexRoute()
    {
        return route('permissions.index');
    }

    /** @test */
    public function user_isnt_admin_cant_see_delete_button_permission()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertDontSeeText('Delete');
    }

    /** @test */
    public function user_is_admin_can_see_delete_button_permission()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText('Delete');
    }

    /** @test */
    public function admin_can_delete_a_permission()
    {
        $this->loginAsAdminRole();
        $permission = Permission::factory()->create();
        $this->delete($this->getDeleteRoute($permission->id));
        $this->assertDatabaseMissing('permissions', $permission->toArray());
    }

    /** @test */
    public function is_not_admin_cant_delete_a_permission()
    {
        $this->loginAsVisitorRole();
        $permission = Permission::factory()->create();
        $response = $this->delete($this->getDeleteRoute($permission->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('permissions', $permission->toArray());
    }
}
