<?php

namespace Tests\Feature\Permissions;

use App\Models\Permission;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdatePermissionTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('permissions.index');
    }

    public function getViewUpdateRoute($id)
    {
        return route('permissions.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('permissions.update', $id);
    }

    /** @test */
    public function user_isnt_admin_cant_see_edit_button_permission()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertDontSeeText('Edit');
    }

    /** @test */
    public function user_is_admin_can_see_edit_button_permission()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText('Edit');
    }

    /** @test */
    public function user_is_admin_can_update_the_permission()
    {
        $this->loginAsAdminRole();
        $permission = Permission::factory()->create();
        $permission->name = "Updated Title Permission";
        $this->put(route('permissions.update', $permission->id), $permission->toArray());
        $this->assertDatabaseHas('permissions', ['id' => $permission->id, 'name' => 'Updated Title Permission']);
    }

    /** @test */
    public function user_is_admin_can_view_update_permission_form()
    {
        $this->loginAsAdminRole();
        $permission = Permission::factory()->create();
        $response = $this->get($this->getViewUpdateRoute($permission->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('permissions.update');
    }

    /** @test */
    public function user_isnt_admin_can_not_see_update_permission_form_view()
    {
        $this->loginAsVisitorRole();
        $permission = Permission::factory()->create();
        $response = $this->get($this->getViewUpdateRoute($permission->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
