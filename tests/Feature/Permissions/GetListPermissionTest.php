<?php

namespace Tests\Feature\Permissions;

use App\Models\Permission;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListPermissionTest extends TestCase
{
    public function getListPermissionRoute()
    {
        return route('permissions.index');
    }

    /** @test */
    public function user_is_visitor_can_get_all_permission()
    {
        $this->loginAsVisitorRole();
        $permission = Permission::factory()->create();
        $response = $this->get($this->getListPermissionRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('permissions.index');
        $response->assertSee($permission->name);
    }
}
