<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('roles.index');
    }

    public function getViewUpdateRoute($id)
    {
        return route('roles.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('roles.update', $id);
    }

    /** @test */
    public function admin_can_update_a_role()
    {
        $this->loginAsAdminRole();
        $roleCreate = Role::factory()->create();
        $roleCreate->name = "Update name role";
        $response = $this->put($this->getUpdateRoute($roleCreate->id), $roleCreate->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', ['id' => $roleCreate->id, 'name' => 'Update name role']);
        $response->assertRedirect($this->getIndexRoute());
    }

    /** @test */
    public function is_not_admin_cant_update_a_role()
    {
        $this->loginAsVisitorRole();
        $roleCreate = Role::factory()->create();
        $roleCreate->name = "Update name role";
        $response = $this->put($this->getUpdateRoute($roleCreate->id), $roleCreate->toArray());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_isnt_admin_cant_see_edit_button_role()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertDontSeeText('Edit');
    }

    /** @test */
    public function user_is_admin_can_see_edit_button_role()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText('Edit');
    }

    /** @test */
    public function user_isnt_admin_cant_see_edit_page()
    {
        $this->loginAsVisitorRole();
        $roleCreate = Role::factory()->create();
        $response = $this->get($this->getViewUpdateRoute($roleCreate->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_isnt_admin_can_see_edit_page()
    {
        $this->loginAsAdminRole();
        $roleCreate = Role::factory()->create();
        $response = $this->get($this->getViewUpdateRoute($roleCreate->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.update');
    }
}
