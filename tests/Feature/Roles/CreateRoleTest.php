<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('roles.index');
    }

    public function getCreateRoleRoute()
    {
        return route('roles.store');
    }

    public function getCreateRoleViewRoute()
    {
        return route('roles.create');
    }

    /** @test */
    public function user_isnt_admin_cant_see_create_role_button()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Create New Role');
    }

    /** @test */
    public function user_is_admin_can_see_create_role_button()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertSeeText('Create New Role');
    }

    /** @test */
    public function admin_can_create_new_role()
    {
        $this->loginAsAdminRole();
        $roleCreate = Role::factory()->make()->toArray();
        $response = $this->post($this->getCreateRoleRoute(), $roleCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $roleCreate);
        $response->assertRedirect($this->getIndexRoute());
    }

    /** @test */
    public function admin_is_admin_can_view_create_role_form()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getCreateRoleViewRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.create');
    }

    /** @test */
    public function user_is_admin_can_not_create_role_if_name_feild_is_null()
    {
        $this->loginAsAdminRole();
        $roleCreate = Role::factory()->make(['name' => null]);
        $response = $this->post($this->getCreateRoleViewRoute(), $roleCreate->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
}
