<?php

namespace Tests\Feature\Roles;

use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    public function getListRoleRoute()
    {
        return route('roles.index');
    }

    /** @test */
    public function unauthorized_user_cant_see_index_role_page()
    {
        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_is_visitor_can_get_all_role()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
    }
}
