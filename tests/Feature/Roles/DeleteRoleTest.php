<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('roles.destroy', $id);
    }

    public function getIndexRoute()
    {
        return route('roles.index');
    }

    /** @test */
    public function admin_can_delete_a_role()
    {
        $this->loginAsAdminRole();
        $roleCreate = Role::factory()->create();
        $this->delete($this->getDeleteRoute($roleCreate->id));
        $this->assertDatabaseMissing('roles', $roleCreate->toArray());
    }

    /** @test */
    public function is_not_admin_cant_delete_a_role()
    {
        $this->loginAsVisitorRole();
        $roleCreate = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoute($roleCreate->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('roles', $roleCreate->toArray());
    }

    /** @test */
    public function user_isnt_admin_cant_see_delete_button_role()
    {
        $this->loginAsVisitorRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertDontSeeText('Delete');
    }

    /** @test */
    public function user_is_admin_can_see_delete_button_role()
    {
        $this->loginAsAdminRole();
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText('Delete');
    }
}
