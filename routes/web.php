<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('/admin/users', UserController::class)
    ->middleware('can:manage-users');

Route::prefix('admin')->middleware('auth')->group(function () {
    //--------------------------------------------Role------------------------------------------//
    Route::prefix('/role')->name('roles.')->middleware('hasRole:admin,visitor')->group(function () {
        Route::get('/', [RoleController::class, 'index'])->name('index')->middleware('hasPermission:role.view');
        Route::get('/create', [RoleController::class, 'create'])->name('create')->middleware('hasPermission:role.create');
        Route::post('/create', [RoleController::class, 'store'])->name('store')->middleware('hasPermission:role.create');
        Route::get('/edit/{id}', [RoleController::class, 'edit'])->name('edit')->middleware('hasPermission:role.update');
        Route::put('/edit/{id}', [RoleController::class, 'update'])->name('update')->middleware('hasPermission:role.update');
        Route::delete('/delete/{id}', [RoleController::class, 'destroy'])->name('destroy')->middleware('hasPermission:role.delete');
    });

    //----------------------------------------------Permission----------------------------------//
    Route::prefix('/permission')->name('permissions.')->middleware('hasRole:admin,visitor')->group(function () {
        Route::get('/', [PermissionController::class, 'index'])->name('index')->middleware('hasPermission:permission.view');
        Route::get('/create', [PermissionController::class, 'create'])->name('create')->middleware('hasPermission:permission.create');
        Route::post('/create', [PermissionController::class, 'store'])->name('store')->middleware('hasPermission:permission.create');
        Route::get('/edit/{id}', [PermissionController::class, 'edit'])->name('edit')->middleware('hasPermission:permission.update');
        Route::put('/edit/{id}', [PermissionController::class, 'update'])->name('update')->middleware('hasPermission:permission.update');
        Route::delete('/delete/{id}', [PermissionController::class, 'destroy'])->name('destroy')->middleware('hasPermission:permission.delete');
    });

    //---------------------------------------------Category---------------------------------------//
    Route::prefix('/category')->name('categories.')->middleware('hasRole:admin,visitor')->group(function () {
        Route::get('/', [CategoryController::class, 'index'])->name('index')->middleware('hasPermission:category.view');
        Route::get('/create', [CategoryController::class, 'create'])->name('create')->middleware('hasPermission:category.create');
        Route::post('/create', [CategoryController::class, 'store'])->name('store')->middleware('hasPermission:category.create');
        Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('edit')->middleware('hasPermission:category.update');
        Route::put('/edit/{id}', [CategoryController::class, 'update'])->name('update')->middleware('hasPermission:category.update');
        Route::delete('/delete/{id}', [CategoryController::class, 'destroy'])->name('destroy')->middleware('hasPermission:category.delete');
    });

    //---------------------------------------------Product-----------------------------------------//
    Route::prefix('/product')->name('products.')->middleware('hasRole:admin,visitor')->group(function () {
        Route::get('/', [ProductController::class, 'index'])->name('index')->middleware('hasPermission:product.view');
        Route::post('/store', [ProductController::class, 'store'])->name('store')->middleware('hasPermission:product.create');
        Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('edit')->middleware('hasPermission:product.update');
        Route::post('/update', [ProductController::class, 'update'])->name('update')->middleware('hasPermission:product.update');
        Route::delete('/delete/{id}', [ProductController::class, 'destroy'])->name('destroy')->middleware('hasPermission:product.delete');
    });
});
