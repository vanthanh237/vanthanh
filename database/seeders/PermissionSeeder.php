<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission1 = Permission::create([
            'name' => 'permission.view'
        ]);

        $permission2 = Permission::create([
            'name' => 'permission.create'
        ]);

        $permission3 = Permission::create([
            'name' => 'permission.update'
        ]);

        $permission4 = Permission::create([
            'name' => 'permission.delete'
        ]);

        $permission5 = Permission::create([
            'name' => 'user.view'
        ]);

        $permission6 = Permission::create([
            'name' => 'user.create'
        ]);

        $permission7 = Permission::create([
            'name' => 'user.update'
        ]);

        $permission8 = Permission::create([
            'name' => 'user.delete'
        ]);
    }
}
