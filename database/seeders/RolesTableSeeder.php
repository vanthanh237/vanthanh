<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create([
            'name' => 'admin'
        ]);

        $admin->permissions()->attach([1,2,3,4,5,6,7,8]);

        $visitor = Role::create([
            'name' => 'visitor'
        ]);

        $visitor->permissions()->attach([1]);
    }
}
